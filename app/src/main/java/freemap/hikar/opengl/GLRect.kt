/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import java.nio.ByteBuffer
import java.nio.ShortBuffer
import java.nio.FloatBuffer
import java.nio.ByteOrder

class GLRect(val vertices: FloatArray, private val colour: FloatArray?){

    private val vertexBuffer: FloatBuffer
    private val indexBuffer: ShortBuffer


    init {
        val buf = ByteBuffer.allocateDirect(12 * 4)
        buf.order(ByteOrder.nativeOrder())
        vertexBuffer = buf.asFloatBuffer()
        val ibuf = ByteBuffer.allocateDirect(6 * 2)
        ibuf.order(ByteOrder.nativeOrder())
        indexBuffer = ibuf.asShortBuffer()

        val indices = shortArrayOf(0, 1, 2, 2, 3, 0)


        vertexBuffer.put(vertices)
        indexBuffer.put(indices)
        vertexBuffer.position(0)
        indexBuffer.position(0)

    }

    fun draw(gpu: GPUInterface) {
        if (colour != null) {
            gpu.setUniform4fv("uColour", colour)
        }
        gpu.drawBufferedData(vertexBuffer, indexBuffer, 12, "aVertex")

    }
}
