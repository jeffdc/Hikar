/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import android.opengl.Matrix
import java.util.ArrayList


open class CompoundModel(val x: Float, val y: Float, val z: Float) {

    class ComponentModel(val a: Float, val model: Model, private val verticalDisplacement: Float = 0.0f) {
        val rotMtx = FloatArray(16)

        init {

            Matrix.setIdentityM(rotMtx, 0)
            Matrix.rotateM(rotMtx, 0, a, 0.0f, 0.0f, 1.0f)
        }

        fun oneTimeScale(factor: Float) {
            model.scale(factor)
            Matrix.translateM(rotMtx, 0, 0.0f, 0.0f, verticalDisplacement * factor)
        }
    }

    protected val models = ArrayList<ComponentModel>()

    protected val modelMtx = FloatArray(16)


    init {

        Matrix.setIdentityM(modelMtx, 0)
        Matrix.translateM(modelMtx, 0, x, y, z)
    }

    fun addModel(m: Model, rotation: Float, dy: Float) {
        models.add(ComponentModel(rotation, m, dy))
    }


    open fun scale(factor: Float) {
        models.forEach { it.oneTimeScale(factor) }
    }

    open fun buffer() {
        models.forEach { it.model.buffer() }
    }

    open fun draw(gpu: GPUInterface) {

        gpu.select()
        gpu.sendMatrix(modelMtx, "uModelMtx")

        models.forEach {

            gpu.sendMatrix(it.rotMtx, "uArmMtx")
            it.model.draw(gpu)
        }
    }
}
