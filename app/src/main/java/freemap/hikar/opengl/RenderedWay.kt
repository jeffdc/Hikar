/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import freemap.data.Point
import freemap.data.Way

import java.nio.ByteBuffer
import java.nio.ShortBuffer
import java.nio.FloatBuffer
import java.nio.ByteOrder
import kotlin.collections.List


class RenderedWay(w: Way? = null, width: Float, route: List<Point>? = null) {

    private var vertexBuffer: FloatBuffer? = null
    private var indexBuffer: ShortBuffer? = null

    private var valid = false
    var isDisplayed = false
        private set

    private var colour = road


    companion object {

        val colours = hashMapOf<String, FloatArray>()
        val road = floatArrayOf(1.0f, 1.0f, 1.0f, 1.0f)


        init {
            val path = floatArrayOf(0.0f, 1.0f, 0.0f, 1.0f)
            val bridleway = floatArrayOf(1.0f, 0.75f, 0.0f, 1.0f)
            val track = floatArrayOf(1.0f, 0.5f, 0.0f, 1.0f)
            val cycleway = floatArrayOf(0.0f, 1.0f, 1.0f, 1.0f)
            val byway = floatArrayOf(1.0f, 0.0f, 0.0f, 1.0f)
            colours["designation:public_footpath"] = path
            colours["designation:public_bridleway"] = bridleway
            colours["designation:public_byway"] = byway
            colours["designation:restricted_byway"] = byway
            colours["designation:byway_open_to_all_traffic"] = byway
            colours["highway:footway"] = path
            colours["highway:path"] = path
            colours["highway:bridleway"] = bridleway
            colours["highway:track"] = track
            colours["highway:cycleway"] = cycleway
        }
    }


    init {

        if (route != null) {
            if (route.size >= 2) {
                colour = floatArrayOf(1.0f, 1.0f, 0.0f, 0.7f)
                createRenderedWay(route, width)
            }
        } else if (w != null) {
            val includedPoints = arrayListOf<Point>()
            for (i in 0 until w.nPoints())
                if (w.getPoint(i).z >= -0.9) //NW 280614 delete this check to see if this is causing problems, 240215 undelete again should be ok
                    includedPoints.add(w.getPoint(i))

            if (includedPoints.size >= 2) {
                val highway = w.getValue("highway")
                val designation = w.getValue("designation")
                if (designation != null && colours["designation:$designation"] != null) {
                    colour = colours["designation:$designation"]!!
                } else {
                    if (colours["highway:$highway"] != null) {
                        colour = colours["highway:$highway"]!!
                    }
                }
                createRenderedWay(includedPoints, width)
            }
        }
    }

    private fun createRenderedWay(points: List<Point>, width: Float) {
        var dx: Float
        var dy: Float
        var dxperp = 0.0f
        var dyperp = 0.0f
        var len: Float
        val nPts = points.size
        val buf = ByteBuffer.allocateDirect(nPts * 6 * 4)
        buf.order(ByteOrder.nativeOrder())
        vertexBuffer = buf.asFloatBuffer()

        val ibuf = ByteBuffer.allocateDirect((nPts - 1) * 6 * 2)
        ibuf.order(ByteOrder.nativeOrder())
        indexBuffer = ibuf.asShortBuffer()

        val vertices = FloatArray(nPts * 6)
        val indices = ShortArray((nPts - 1) * 6)

        isDisplayed = true




        for (i in 0 until nPts - 1) {

            val thisPoint = points[i]
            val nextPoint = points[i + 1]


            dx = (nextPoint.x - thisPoint.x).toFloat()
            dy = (nextPoint.y - thisPoint.y).toFloat()
            len = thisPoint.distanceTo(nextPoint).toFloat()

            dxperp = -(dy * (width / 2)) / len
            dyperp = (dx * (width / 2)) / len
            vertices[i * 6] = (thisPoint.x + dxperp).toFloat()
            vertices[i * 6 + 1] = (thisPoint.y + dyperp).toFloat()
            vertices[i * 6 + 2] = thisPoint.z.toFloat()
            vertices[i * 6 + 3] = (thisPoint.x - dxperp).toFloat()
            vertices[i * 6 + 4] = (thisPoint.y - dyperp).toFloat()
            vertices[i * 6 + 5] = thisPoint.z.toFloat()

        }
        val k = nPts - 1
        // TODO fix this replace TileProjectionDisplayTransformation
        val thisPoint = points[k]
        vertices[k * 6] = (thisPoint.x + dxperp).toFloat()
        vertices[k * 6 + 1] = (thisPoint.y + dyperp).toFloat()
        vertices[k * 6 + 2] = thisPoint.z.toFloat()
        vertices[k * 6 + 3] = (thisPoint.x - dxperp).toFloat()
        vertices[k * 6 + 4] = (thisPoint.y - dyperp).toFloat()
        vertices[k * 6 + 5] = thisPoint.z.toFloat()

        for (i in 0 until nPts - 1) {
            indices[i * 6] = (i * 2).toShort()
            indices[i * 6 + 1] = (i * 2 + 1).toShort()
            indices[i * 6 + 2] = (i * 2 + 2).toShort()
            indices[i * 6 + 3] = (i * 2 + 1).toShort()
            indices[i * 6 + 4] = (i * 2 + 3).toShort()
            indices[i * 6 + 5] = (i * 2 + 2).toShort()
        }

        vertexBuffer?.put(vertices)
        indexBuffer?.put(indices)
        vertexBuffer?.position(0)
        indexBuffer?.position(0)


        valid = true
    }


    fun draw(gpuInterface: GPUInterface) {

        if (valid) {
            gpuInterface.setUniform4fv("uColour", colour)
            gpuInterface.drawBufferedData(vertexBuffer!!, indexBuffer!!, 12, "aVertex")
        }
    }

    private fun getAveragePosition(): Point? {
        if (valid) {
            val p = Point()
            val nPoints = vertexBuffer!!.limit() / 3
            for (i in 0 until vertexBuffer!!.limit() step 3) {
                p.x += vertexBuffer!![i]
                p.y += vertexBuffer!![i + 1]
            }
            p.x /= nPoints
            p.y /= nPoints

            return p
        }
        return null
    }

    fun distanceTo(p: Point): Double {

        val av = getAveragePosition()
        return av?.distanceTo(p) ?: -1.0
    }
}
