/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl
import android.opengl.GLES20

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer

class Model(private val verticesAndTexcoords: FloatArray, val indices: ShortArray) {
    private var vertexAndTexBuffer : FloatBuffer? = null
    private var indexBuffer: ShortBuffer? = null


    fun scale(factor: Float) {
        val recordSize = 5
        for(i in 0 until verticesAndTexcoords.size step recordSize) {
            verticesAndTexcoords[i] *= factor
            verticesAndTexcoords[i+1] *= factor
            verticesAndTexcoords[i+2] *= factor
        }
    }

    fun buffer() {
        val  buffer = ByteBuffer.allocateDirect(verticesAndTexcoords.size * 4)
        buffer.order(ByteOrder.nativeOrder())
        vertexAndTexBuffer = buffer.asFloatBuffer()

        val iBuffer = ByteBuffer.allocateDirect(indices.size * 2)
        iBuffer.order(ByteOrder.nativeOrder())
        indexBuffer = iBuffer.asShortBuffer()

        vertexAndTexBuffer?.put(verticesAndTexcoords)
        indexBuffer?.put(indices)

    }
    
    fun draw(gpu: GPUInterface) {
       gpu.drawTexturedBufferedData(vertexAndTexBuffer!!, indexBuffer!!, "aVertex", "aTexCoord", GLES20.GL_TRIANGLES)
    }
}
