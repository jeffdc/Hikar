/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.signposting

import freemap.data.*
import freemap.datasource.FreemapDataset
import freemap.hikar.datasource.OsmDemIntegrator
import java.lang.IndexOutOfBoundsException
import kotlin.collections.ArrayList


fun Way.isWalkingRoute(strict: Boolean = false): Boolean {
    return ((arrayListOf(
        "footway",
        "path",
        "bridleway",
        "cycleway",
        "steps",
        "service",
        "track"
    ).contains(tags["highway"]) &&
            ((!strict && tags["foot"] != "private") || (strict && arrayListOf(
                "yes",
                "permissive",
                "designated",
                "public"
            ).contains(tags["foot"]))) &&
            tags["access"] != "private") ||
            arrayListOf(
                "public_footpath",
                "public_bridleway",
                "byway_open_to_all_traffic",
                "restricted_byway"
            ).contains(tags["designation"]))
}

fun Way.isWalkableRoad(): Boolean {
    return listOf(
        "trunk",
        "trunk_link",
        "primary",
        "primary_link",
        "secondary",
        "secondary_link",
        "tertiary",
        "tertiary_link",
        "unclassified",
        "unclassified_link",
        "residential",
        "residential_link"
    ).contains(tags["highway"])
}

class GraphManager(private val threshold: Double, private val poiLineThreshold: Double) {

    val graph = Graph()
    var graphLoaded = false
    private var tiles = OsmDemIntegrator.KeyedOSMTiles()
    private val unprojectedWays = arrayListOf<LocalIdWay>()


    class GraphManagerException(val msg: String) : Exception()


    data class JunctionNode(val vtxIdx: Int, val idx: Int)
    data class LocalIdWay(val localId: Int, val way: Way)
    data class Intersection(val p: Point, val prop: Double)
    data class PoiLoc(val p: Point, val unprojected: Point)

    fun makeGraph(tiles: OsmDemIntegrator.KeyedOSMTiles) {
        graphLoaded = false
        this.tiles = tiles
        val junctionNodes = arrayListOf<ArrayList<JunctionNode>>()

        val map = tiles.tilesAsMap()

        val boundingBoxes = arrayListOf<BoundingBox>()
        unprojectedWays.clear()
        val allPoiLocs = arrayListOf<PoiLoc>()

        // We MUST create our unprojected ways - even if we are loading a saved graphMgr
        map.entries.forEach { entry ->


            val dataset = entry.value as FreemapDataset



            dataset.ways.sortBy { it.id } // TODO do this at point where dataset is loaded

            dataset.ways.forEach { w ->
                if (w.isWalkingRoute() || w.isWalkableRoad()) {
                    unprojectedWays.add(LocalIdWay(unprojectedWays.size, w.unprojectedWay))
                    junctionNodes.add(arrayListOf())
                    boundingBoxes.add(w.boundingBox)
                }
            }
        }


        // Add unprojected locations of all POIs
        // 130919 not doing POIs, just annotations (performance/accuracy tradeoff)
        /*
        val iterator = tiles.poiIterator()

        var poi = iterator.next() as POI?
        while (poi != null) {
            if (poi.isForSignpost()) {
                allPoiLocs.add(PoiLoc(poi.point, poi.unprojectedPoint))
            }
            poi = iterator.next() as POI?
        }
        */
        val annIterator = tiles.annotationIterator()
        var a = annIterator.next() as freemap.data.Annotation?
        while (a != null) {
            allPoiLocs.add(PoiLoc(a.point, a.unprojectedPoint))
            a = annIterator.next() as freemap.data.Annotation?
        }
        //     Log.d("hikar", "Found ${unprojectedWays.size} unprojected ways and ${allPoiLocs.size} POIs.")

        graph.clear()

        //*** NEW
        // First thing we need to do is insert the POIs to the ways, worry about making the graph later.
        // We don't find the nearest way, just the first one within the threshold. As long as the threshold is small,
        // this shouldn't matter. Trying to avoid all this slowing down the graph creation.

        unprojectedWays.forEachIndexed { i, w ->
            val newPoints = arrayListOf<Point>()
            val intersections = arrayListOf<Intersection>()


            w.way.points.forEachIndexed { origWayNodeIdx, p ->
                newPoints.add(p)
                if (origWayNodeIdx < w.way.points.size - 1) {
                    // find all POIs within bounding box plus 10 metre buffer (to be safe...)
                    val nearbyPoiLocs = allPoiLocs.filter {
                        it.p.x in boundingBoxes[i].bottomLeft.x - 10..boundingBoxes[i].topRight.x + 10 && it.p.y in boundingBoxes[i].bottomLeft.y - 10..boundingBoxes[i].topRight.y + 10
                    }

                    intersections.clear()

                    // all POIs close to this way
                    nearbyPoiLocs.forEach {

                        // Would a line perpendicular to the current way segment intersect it? (prop measures this)

                        val intersection = Point()

                        val prop =
                            it.unprojected.getIntersection(
                                w.way.points[origWayNodeIdx],
                                w.way.points[origWayNodeIdx + 1],
                                intersection
                            )
                        //  Log.d("hikar", "prop is $prop")
                        // If so...
                        if (prop > 0.0 && prop < 1.0) {
                            // Distance from point to intersection
                            val dist = Algorithms.haversineDist(
                                it.unprojected.x,
                                it.unprojected.y,
                                intersection.x,
                                intersection.y
                            )
                            // If POI is within dist, add to list of intersections - store prop - we need this for sorting
                            //      Log.d("hikar", "POI dist is $dist")
                            if (dist < poiLineThreshold) {
                                intersection.z = Double.MAX_VALUE // trick to mark as junction
                                intersections.add(Intersection(intersection, prop))
                            }
                        }
                    }

                    // sort the intersections by order along the segment
                    intersections.sortBy { it.prop }

                    // loop through each intersection, add each to the way as a point
                    intersections.forEach {
                        // add it as a node to the way
                        newPoints.add(it.p)
                    }
                }
            }


            w.way.points = newPoints
        }

        // Now we've inserted all the POIs into our unprojected ways, generate the graph as we always have...
        unprojectedWays.forEach { w ->


            val nearbyWays =
                unprojectedWays.filter { nw ->
                    try {
                        nw.localId > w.localId && boundingBoxes[nw.localId].intersects(
                            boundingBoxes[w.localId],
                            5.0
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        val gme =
                            GraphManagerException("index out of bounds: unproj ways size ${unprojectedWays.size} bounding box size ${boundingBoxes.size} i ${w.localId} j ${nw.localId}")
                        gme.stackTrace = e.stackTrace
                        throw gme
                    }
                }

            var wayNodeIndex = 0

            w.way.points.forEach { p ->
                var vtxIdx = 0

                if (p.z == Double.MAX_VALUE) {
                    vtxIdx = graph.addVertex(Point(p.x, p.y), true)
                    junctionNodes[w.localId].add(JunctionNode(vtxIdx, wayNodeIndex))
                } else {
                    var hits = false
                    lateinit var node: Point

                    nearbyWays.forEach { nw ->

                        val distInfo = nw.way.findNearestPointTo(p)

                        if (distInfo.distance < threshold) {

                            if (!hits) {
                                node = Point(p.x, p.y)
                                vtxIdx = graph.addVertex(node)
                                junctionNodes[w.localId].add(JunctionNode(vtxIdx, wayNodeIndex))

                                hits = true

                            }

                            junctionNodes[nw.localId].add(JunctionNode(vtxIdx, distInfo.index))
                        }
                    }
                }

                wayNodeIndex++
            }
        }

        junctionNodes.forEachIndexed { localId, holdersForWay ->
            holdersForWay.sortBy { it.idx }
            val distinctHolders = holdersForWay.distinctBy { it.idx }
            distinctHolders.forEachIndexed { i, _ ->
                var dist = 0.0

                if (i < distinctHolders.size - 1) {

                    for (idx in distinctHolders[i].idx until distinctHolders[i + 1].idx) {
                        dist += Algorithms.haversineDist(
                            unprojectedWays[localId].way.getPoint(idx).x,
                            unprojectedWays[localId].way.getPoint(idx).y,
                            unprojectedWays[localId].way.getPoint(idx + 1).x,
                            unprojectedWays[localId].way.getPoint(idx + 1).y
                        )
                    }

                    graph.addEdge(
                        distinctHolders[i].vtxIdx,
                        distinctHolders[i + 1].vtxIdx,
                        EdgeData(
                            unprojectedWays[localId],
                            localId,
                            dist,
                            distinctHolders[i].idx,
                            distinctHolders[i + 1].idx,
                            unprojectedWays[localId].way.isWalkingRoute()
                        )
                    )
                }
            }
        }
        graphLoaded = true
    }


    // 200.0 will renderedRoute to Telegraph Hill, 100.0 will not

    fun calcRoute(
        start: Point,
        end: Point,
        endThreshold: Double = 200.0,
        doStart: Boolean = false,
        startThreshold: Double = 10.0
    ): Route {

        val path = graph.dijkstra(start, end)
        val route = Route(path.numJunctions)

        // If the path is empty, use the nearest vertex to the start for the 'endVertex'
        // This is in case the destination point is before the end of the first segment in that direction
        // (likely to be the case for an annotation) and a route cannot be found; we can still walk along the
        // edges of the vertex to find the end point.
        val startVertex = graph.findNearestVertex(start)
        var endVertex = startVertex

        if (!path.isEmpty()) {


            route.initialBearing = path.edges[0].getInitialBearing()

            path.edges.forEachIndexed { i, edge ->
                //    Log.d("hikar", "Adding segment: ${edge.data.localIdWay.way.id} ${edge.data.dist} from ${edge.data.fromIdx} to ${edge.data.toIdx}")
                route.addSegment(
                    path.vertices[i].p,
                    path.vertices[i + 1].p,
                    path.edges[i].data.localIdWay.way.id,
                    edge.data.dist,
                    edge.data.isWalkingRoute,
                    edge.getPoints()
                )
            }
            endVertex = path.vertices.last()
        }

        // If the POI has no vertex (was too far from a way), find the route to the nearest node to the POI
        if (endVertex != null && endVertex.p != end) {
            addNonEdgeSegments(endVertex, end, endThreshold, path, route)
        }

        // add route from current point to start vertex if required (e.g. when routing to a POI)
        if (doStart && startVertex != null) {
            addNonEdgeSegments(startVertex, start, startThreshold, path, route, true)
        }
        return route
    }


    private fun addNonEdgeSegments(
        consideredVertex: Vertex,
        consideredPoint: Point,
        threshold: Double,
        path: Path,
        route: Route,
        isStart: Boolean = false

    ) {
        var lowestDist = Double.MAX_VALUE
        var closestEdge: Graph.Edge? = null
        var closestEdgeNonExact: Graph.Edge? = null
        var closestExactPointIdx = -1
        var closestNonExactPointIdx = -1
        var lowestDistNonExact = Double.MAX_VALUE

        // First, find the closest way node to the considered point, within the threshold distance.
        // Also find the closest "non exact point" if we're considering the start of the route.
        // This involves finding the closest node-node line to the point under consideration and then
        // selecting the node of this pair closest to the consideredVertex.
        // We then have a segment from this node to the consideredVertex and can then add on a segment from the
        // consideredPoint to this node.

        // 200819 Issue with 'exact' search is that if we're trying to find the nearest edge to our current GPS location,
        // we find the edge with the nearest POINT (OSM NODE) - not what we want. So always find the nearest ACTUAL
        // EDGE SEGMENT with haversineDistToLine(), certainly if isStart (i.e. the 'route to POI' functionality).
        // Note for personal use - before fixing, this failed (found the wrong edge) at the Acres Down junction with the 'green' path west of the car park.

        consideredVertex.edges.forEach {

            var dist: Double
            var distNonExact: Double
            val curEdgeProgression = it.progressionFrom(consideredVertex.p)


            for (i in it.progressionFrom(consideredVertex.p)) {

                if (!isStart) {
                    val pointOnWay = it.data.localIdWay.way.getPoint(i)
                    dist =
                        Algorithms.haversineDist(pointOnWay.x, pointOnWay.y, consideredPoint.x, consideredPoint.y)

                    if (dist < lowestDist) {
                        if (dist < threshold) {
                            lowestDist = dist
                            closestExactPointIdx = i
                        }
                        closestEdge = it
                    }
                } else if (i != curEdgeProgression.last /*&& isStart*/) {

                    distNonExact = consideredPoint.haversineDistToLine(
                        it.data.localIdWay.way.getPoint(i),
                        it.data.localIdWay.way.getPoint(i + curEdgeProgression.step)
                    )

                    if (distNonExact >= 0 && distNonExact < lowestDistNonExact) {
                        lowestDistNonExact = distNonExact
                        // The point to use will be the second one if the edge is the first edge of the existing path
                        // (because we will need to remove the edge and replace it with a route to the target of the
                        // first edge, the one being removed) or the first one otherwise (as we need to add a new segment to the beginning
                        // of the first edge of the existing path)
                        closestNonExactPointIdx =
                            if ((!path.isEmpty() && (isStart && it == path.edges.first()) || (!isStart && it.isReverseOf(
                                    path.edges.last()
                                )))
                            ) i + curEdgeProgression.step else i
                        closestEdgeNonExact = it
                    }
                }
            }
        }

        val le = if (closestExactPointIdx > -1) closestEdge else closestEdgeNonExact


        if (le != null) {
            val closestPointIdx = if (closestExactPointIdx > -1) closestExactPointIdx else closestNonExactPointIdx
            if (closestPointIdx > -1) {
                var extraDist = 0.0
                val remove = !path.isEmpty() && shouldRemove(le, path, isStart)
                val sourceIdx = le.getIndex(consideredVertex.p)
                val targetIdx = le.getIndex(le.target.p)
                val closestWayPoint = le.data.localIdWay.way.getPoint(closestPointIdx)
                // ***160719*** if the lowestEdge is the last edge on the route we want to count from the PREVIOUS-to-end vertex to the POI, not the consideredVertex
                var idxWalk =
                    if (remove) (if (isStart) closestPointIdx else targetIdx) else (if (isStart) closestPointIdx else sourceIdx)
                val idxEnd =
                    if (remove) (if (isStart) targetIdx else closestPointIdx) else (if (isStart) sourceIdx else closestPointIdx)

                val step = if (idxWalk > idxEnd) -1 else 1
                val extraAsPoints = arrayListOf<Point>()
                if (idxWalk != idxEnd) {
                    extraAsPoints.add(le.data.localIdWay.way.getPoint(idxWalk))
                }
                while (idxWalk != idxEnd) {
                    extraDist += Algorithms.haversineDist(
                        le.data.localIdWay.way.getPoint(idxWalk).x,
                        le.data.localIdWay.way.getPoint(idxWalk).y,
                        le.data.localIdWay.way.getPoint(idxWalk + step).x,
                        le.data.localIdWay.way.getPoint(idxWalk + step).y
                    )
                    extraAsPoints.add(le.data.localIdWay.way.getPoint(idxWalk + step))
                    idxWalk += step
                }


                if (extraDist > 0) {

                    /// ***160719*** isReverseOf()
                    if (remove) {
                        //     Log.d("hikar", "Removing segment at : ${getSegmentToRemove(route, isStart)}")
                        route.segments.removeAt(getSegmentToRemove(route, isStart))
                        //   Log.d("hikar", "Adding replacement segment")

                        route.addSegment(
                            //    le.e.getOtherNode(consideredVertex.p),
                            if (isStart) closestWayPoint else le.target.p,
                            //     unprojectedWays[le.data.localId].localIdWay.getPoint(lowestPointIdx),
                            if (isStart) le.target.p else closestWayPoint,
                            // le.data.wayId,
                            le.data.localIdWay.way.id,
                            le.data.dist - extraDist,
                            le.data.isWalkingRoute,
                            extraAsPoints, isStart
                        )
                    } else {
                        //       Log.d("hikar", "The selected edge is NOT the last edge of the renderedRoute.")
                        route.addSegment(
                            if (isStart) closestWayPoint else consideredVertex.p,
                            if (isStart) consideredVertex.p else closestWayPoint,
                            le.data.localIdWay.way.id,
                            extraDist,
                            le.data.isWalkingRoute,
                            extraAsPoints, isStart
                        )

                        // If we are extending the route beyond the considered vertex, and the considered vertex is a junction,
                        // and consideredVertex is not the start of the path (will be the case if we're routing to somewhere
                        // between the first and second vertices of the path) add one to numJunctions in the route
                        if (consideredVertex.edges.size >= 3 && (path.vertices.isEmpty() || consideredVertex != path.vertices.first())) {
                            route.numJunctions++
                        }
                    }
                }

            }

            // if the start point was not sufficiently near a way node, we need to add a further segment between the
            // start point and the closest "non exact" way node, i.e. the way node nearest the start point and in
            // the direction of consideredVertex (for how this is obtained, see above)
            if (isStart && route.segments.isNotEmpty() && closestExactPointIdx == -1 && closestNonExactPointIdx > -1) {
                val closestNonExactPoint = le.data.localIdWay.way.getPoint(closestNonExactPointIdx)
                val extraDist = Algorithms.haversineDist(
                    consideredPoint.x,
                    consideredPoint.y,
                    closestNonExactPoint.x,
                    closestNonExactPoint.y
                )
                route.addSegment(
                    consideredPoint,
                    closestNonExactPoint,
                    le.data.localIdWay.way.id,
                    extraDist,
                    le.data.isWalkingRoute,
                    arrayListOf(consideredPoint, closestNonExactPoint), true
                )
            }
        }
    }

    private fun getSegmentToRemove(route: Route, isStart: Boolean): Int {
        return if (isStart) 0 else route.segments.size - 1
    }

    private fun shouldRemove(le: Graph.Edge, path: Path, isStart: Boolean): Boolean {
        return if (isStart) le == path.edges.first() else le.isReverseOf(path.edges.last())
    }


    fun routeToPoi(startPoint: Point, poi: POI): List<Point> {
        val route = calcRoute(startPoint, poi.unprojectedPoint, 200.0, true)
        return route.asPoints()
    }
}

