package freemap.hikar.signposting

import freemap.data.Point


class CompoundNoticeBoard (override val position: Point): Sign {

    private val noticeBoards = arrayListOf<NoticeBoard>()

    fun addNoticeBoard(board: NoticeBoard) {
        noticeBoards.add(board)
    }
    override fun getSignPlates(): Array<SignPlate> {
        return noticeBoards.toTypedArray()
    }

    override fun getVerticalDisplacement(plateIdx: Int) : Float {
        return -22f * plateIdx
    }
}