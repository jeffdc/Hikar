/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.datasource

import freemap.data.GoogleProjection
import freemap.data.Point
import freemap.datasource.*
import freemap.jdem.DEM
import java.lang.RuntimeException

class OsmDemIntegrator(osmUrl: String, demUrl: String) {

    inner class UpdateException(val origException: Exception) : RuntimeException()

    var zoom = 13
        set(value) {
            field = value
            dem.zoom = value
            osm.zoom = value
        }

    private val dem = XYZTileDeliverer(DemDataSource(demUrl), "dem", zoom)
    private var osm = XYZTileDeliverer(OsmDataSource(osmUrl), "osm", zoom)

    val failedDems = listOf<GoogleProjection.Tile>()
    val failedOsms = listOf<GoogleProjection.Tile>()

    private var osmTiles = XYZTileDeliverer.TiledDataInfo()
    private var demTiles = XYZTileDeliverer.TiledDataInfo()

    private var updateError = false

    class KeyedOSMTiles(tiledDataInfo: XYZTileDeliverer.TiledDataInfo) : OSMTiles(tiledDataInfo.tiledData) {
        private var key = ""

        init {
            tiledDataInfo.apply {
                key = "$zoomLevel.$xCentre.$yCentre.$dimension"
            }
        }

        constructor() : this(XYZTileDeliverer.TiledDataInfo())
    }

    fun needNewData(lonLat: Point): Boolean {
        if(!updateError) {
            val osmNeedData = osm.needNewData(lonLat)
            val demNeedData = dem.needNewData(lonLat)
            return osmNeedData || demNeedData
        }
        return true
    }

    fun update(lonLat: Point): Boolean {
        try {
            updateError = false
            osmTiles = osm.getData(lonLat, 3)
            demTiles = dem.getData(lonLat, 3)

            osmTiles.tiledData.forEach {
                (it.value as FreemapDataset).applyDEM(demTiles.tiledData[it.key] as DEM)
            }
            return true
        } catch (e: Exception) {
            updateError = true
            throw UpdateException(e)
        }
    }

    fun getHeight(lonLat: Point): Double {
        var height = -1.0

        if (!needNewData(lonLat)) {
            // Alteration here, originally we projected our lonLat into the DEM's projection.
            // This is unnecessary, as DEM.getHeight() will do it for us
            val dem = dem.getCurrentTile(lonLat) as DEM?
            height = dem?.getHeight(lonLat.x, lonLat.y) ?: 0.0
        }
        return height
    }


    fun getCurrentOSMTiles(): KeyedOSMTiles {
        return KeyedOSMTiles(osmTiles)
    }


    fun getCurrentDEMTiles(): HashMap<String, TiledData?> {
        return demTiles.tiledData
    }

    fun deleteOsmData() {
        osm.deleteData()
    }
}