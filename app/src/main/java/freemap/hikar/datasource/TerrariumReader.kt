/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.datasource

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import freemap.data.GoogleProjection
import freemap.jdem.DEM
import java.io.InputStream
import java.lang.RuntimeException

class TerrariumReader(val t: GoogleProjection.Tile) {

    fun getData(inp: InputStream) : DEM {
        val decoded = BitmapFactory.decodeStream(inp) ?: throw RuntimeException("Unable to decode DEM elevation bitmap")
        return doReadBitmap(decoded)
    }

    fun getData(bytes: ByteArray) : DEM {
        val decoded = BitmapFactory.decodeByteArray(bytes, 0, bytes.size) ?: throw RuntimeException("Unable to decode DEM elevation bitmap")
        return doReadBitmap(decoded)
    }

    private fun doReadBitmap(bmp: Bitmap) : DEM {

        val dem = DEM(t.bottomLeft, t.topRight, bmp.width, bmp.height, GoogleProjection())
        val pixelArray = IntArray(bmp.width*bmp.height)

        bmp.getPixels(pixelArray, 0, bmp.width, 0, 0, bmp.width, bmp.height)

        // mapzen.com/documentation/terrain-tiles/formats
        val elevations = pixelArray.map { ((Color.red(it)*256 + Color.green(it) + Color.blue(it).toFloat()/256) - 32768).toInt() }
        dem.setHeights(elevations.toIntArray())
        dem.setExtrapolateEdges(true)
        return dem
    }
}