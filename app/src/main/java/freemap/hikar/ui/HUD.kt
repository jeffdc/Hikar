/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.ui

import android.app.Activity
import android.graphics.Rect
import android.util.DisplayMetrics
import android.view.View
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Color
import android.content.Context


class HUD(ctx: Context) : View(ctx) {

    private val msgPaint = Paint()
    private val routingMsgPaint = Paint()
    private val transparentPaint = Paint()
    private var msg = ""
    private var routingMsg = ""
    private val msgRect = Rect()
    private val routingMsgRect = Rect()
    private var blankMessage = false
    private var blankRoutingMessage = false
    private val displayMetrics = DisplayMetrics()


    init {

        msgPaint.color = Color.WHITE
        msgPaint.textSize = 96f


        routingMsgPaint.color = Color.YELLOW
        routingMsgPaint.textSize = 72f

        transparentPaint.color = Color.TRANSPARENT

        (ctx as Activity).windowManager.defaultDisplay.getRealMetrics(displayMetrics)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)


        if (msg != "") {
            canvas.drawText(msg, msgRect.left.toFloat(), msgRect.top.toFloat(), msgPaint)
        }

        if (routingMsg != "") {
            canvas.drawText(routingMsg, routingMsgRect.left.toFloat(), routingMsgRect.top.toFloat(), routingMsgPaint)
        }

        if (blankMessage) {
            canvas.drawRect(msgRect, transparentPaint)
            blankMessage = false
        }

        if (blankRoutingMessage) {
            canvas.drawRect(routingMsgRect, transparentPaint)
            blankRoutingMessage = false
        }

    }

    fun removeMessage() {
        msg = ""
        blankMessage = true
        invalidate()
    }

    fun stopRouting() {
        routingMsg = ""
        blankRoutingMessage = true
        invalidate()
    }

    fun setMessage(msg: String) {

        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels
        this.msg = msg
        msgPaint.getTextBounds(msg, 0, msg.length, msgRect)
        msgRect.offsetTo(width / 2 - msgRect.width() / 2, height / 2 - msgRect.height() / 2)
        invalidate()
    }

    fun setRoutingLocation(location: String) {
        routingMsg = " Routing to $location"
        routingMsgPaint.getTextBounds(routingMsg, 0, routingMsg.length, routingMsgRect)
        routingMsgRect.offsetTo(10, 10 + routingMsgRect.height())
        invalidate()
    }
}
