/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.ui

import android.view.MotionEvent
import android.view.View
import kotlin.math.abs
import kotlin.math.sqrt


//see http://www.zdnet.com/blog/burnette/how-to-use-multi-touch-in-android-2-part-3-understanding-touch-events/
//1775?tag=content;siu-container

class PinchListener(private val onPinch: (Int) -> Unit) : View.OnTouchListener {

    private val xDown = floatArrayOf(-1.0f, 1.0f)
    private val xUp = floatArrayOf(-1.0f, -1.0f)
    private val yDown = floatArrayOf(-1.0f, -1.0f)
    private val yUp = floatArrayOf(-1.0f, -1.0f)
    private var dragging = false

    companion object {
        const val PINCH_IN = 1
        const val PINCH_OUT = -1
    }


    override fun onTouch(view: View, ev: MotionEvent): Boolean {

        val action = ev.action and MotionEvent.ACTION_MASK
        val ptrIdx : Int

        when (action) {

            MotionEvent.ACTION_POINTER_DOWN -> {
                if (ev.pointerCount == 2) {
                    for (i in 0 until 2) {
                        xDown[ev.getPointerId(i)] = ev.getX(i)
                        yDown[ev.getPointerId(i)] = ev.getY(i)
                    }
                    dragging = true
                }
            }

            MotionEvent.ACTION_POINTER_UP -> {
                if (dragging) {
                    ptrIdx = ev.action shr MotionEvent.ACTION_POINTER_INDEX_SHIFT
                    xUp[ev.getPointerId(ptrIdx)] = ev.getX(ptrIdx)
                    yUp[ev.getPointerId(ptrIdx)] = ev.getY(ptrIdx)

                }
            }

            MotionEvent.ACTION_UP -> {
                if (dragging) {
                    ptrIdx = ev.action shr MotionEvent.ACTION_POINTER_INDEX_SHIFT
                    xUp[ev.getPointerId(ptrIdx)] = ev.x
                    yUp[ev.getPointerId(ptrIdx)] = ev.y

                    dragging = false

                    val dxdown = xDown[1] - xDown[0]
                    val dydown = yDown[1] - yDown[0]
                    val ddown = sqrt(dxdown * dxdown + dydown * dydown)
                    val dxup = xUp[1] - xUp[0]
                    val dyup = yUp[1] - yUp[0]
                    val dup = sqrt(dxup * dxup + dyup * dyup)

                    if (abs(ddown - dup) > 50.0f) {
                        if (ddown > dup) {
                            onPinch(PINCH_IN)
                        } else if (ddown < dup) {
                            onPinch(PINCH_OUT)
                        }
                    }
                }
                for (j in 0 until 2) {
                    xDown[j] = -1.0f
                    xUp[j] = -1.0f
                    yDown[j] = -1.0f
                    yUp[j] = -1.0f
                }
            }
        }
        return true
    }
}